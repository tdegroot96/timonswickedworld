<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get login
     * @return array|\Illuminate\View\View
     */
    public function getLogin() {
        return view('auth.login');
    }

    /**
     * Post login
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function postLogin() {
        $username = Input::get('username');
        $email = Input::get('email');
        $password = Input::get('password');

        /** @var \Illuminate\Auth\Guard $auth */
        $auth = auth();
        if ($auth->attempt(['username' => $username, 'password' => $password])) {
            return redirect('/admin');
        }
        return view('auth.login');
    }

    /**
     * @return string
     */
    public function loginUsername() {
        return 'username';
    }

    /**
     * Get register
     * @return array|\Illuminate\View\View
     */
    public function getRegister() {
        return view('auth.register');
    }

    /**
     * Post login
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|void
     */
    public function postRegister() {
        $data = array(
            'username' => Input::get('username'),
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation')
        );

        if (!$this->validator($data)->fails()) {
            $this->create($data);
            return redirect('/auth/login');
        }
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

}
