<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>Timon's Wicked World</title>
        <link href="{{ asset('css/bootstrap.css') }}" type="text/css" rel="stylesheet">
        <link href="{{ asset('css/bootstrap-theme.css') }}" type="text/css" rel="stylesheet">
        <link href="{{ asset('css/main.css') }}" type="text/css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="header">
            <nav class="nav">
                <a class="navbar-title" href="/">
                    <h1>Home</h1>
                </a>
            </nav>
        </div>
        <div class="content">
            @yield('content')
        </div>
    </body>
</html>