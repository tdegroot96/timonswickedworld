@extends('auth.master')
@section('form')
<form class="form form-login text-center" method="POST" action="/auth/register">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-field">
        <input type="text" name="username" value="{{ old('username') }}" placeholder="Name">
    </div>
    <div class="form-field">
        <input type="text" name="email" value="{{ old('email') }}" placeholder="Email">
    </div>
    <div class="form-field">
        <input type="password" name="password" placeholder="Password">
    </div>
    <div class="form-field">
        <input type="password" name="password_confirmation" placeholder="Confirm password">
    </div>
    <div class="form-field">
        <button class="btn" type="submit">Register</button>
    </div>
</form>
@endsection