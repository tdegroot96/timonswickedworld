@extends('auth.master')

@section('form')
<form class="form form-login text-center" method="POST" action="/auth/login">
    {!! csrf_field() !!}
    <div class="form-field">
        <input type="text" name="username" value="{{ old('username') }}" placeholder="Login">
    </div>
    <div class="form-field">
        <input type="password" name="password" id="password" placeholder="Password">
    </div>
    <div class="form-field">
        {{--<input type="checkbox" name="remember"> Remember Me--}}
    </div>
    <div class="form-field">
        <button class="btn" type="submit">Sign in</button>
    </div>
</form>
@endsection